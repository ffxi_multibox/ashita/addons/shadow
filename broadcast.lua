require('common')
local primitives = require('primitives')
local scaling = require('scaling')

require('multibox_common')

multibox = multibox or {}


local startup
local update_broadcast
local update_status_indicator
local create_texture_primitive
local broadcast = 'off'
local broadcast_status_indicator

local repeater_on_path = ('%s\\addons\\%s\\%s'):fmt(AshitaCore:GetInstallPath(), addon.name, 'data/repeater_on.png')
local repeater_off_path = ('%s\\addons\\%s\\%s'):fmt(AshitaCore:GetInstallPath(), addon.name, 'data/repeater_off.png')

local npc_enter_delay = 0.5

ashita.events.register('command', 'broadcast_command_callback', function (e)
    local args = e.command:args()
    local command = table.remove(args, 1)
    local subcommand = table.remove(args, 1)

    -- shadow broadcast|bcast|repeat [on|off] (will toggle if on/off omitted)
    if command == '/shadow' then
        if subcommand == 'broadcast' or subcommand == 'bcast' or subcommand == 'repeat' then
            local msg = 'broadcast'
            if #args >= 1 then
                msg = msg..' '..args[1]
            else
                msg = msg..' '..((broadcast == 'on') and 'off' or 'on')
            end
            ipc.send_message(msg)
        end
    end
end)

local function keypress(key)
    commands.send_command(1, '/sendkey '..key..' down', T{})
    commands.send_command(1, '/sendkey '..key..' up', T{}, 0.1)
end

local function keypress_staggered(key, sender_slot)
    local target = ffxi.get_mob_by_target('t')
    local slot = slots.get_current_window().slot
    if target and target.is_npc and (key == 'return' or key == 'numpadenter') then
        local delay = npc_enter_delay * slot
        ashita.tasks.once(delay, function() keypress(key) end)
    else
        keypress(key)
    end
end

ipc.register(function(args)
    local command = table.remove(args, 1)
    local subcommand = table.remove(args, 1)

    if command == 'broadcast' then
        if subcommand == 'keypress' then
            local key = args[1]
            local sender_slot = tonumber(args[2])
            keypress_staggered(key, sender_slot)
        else
            update_broadcast(subcommand)
        end
    end
end)

ashita.events.register('key_data', 'keyboard_key_data_callback', function (e)
    -- print(e.key)
    if not window.is_active() then return end -- only process input from the active window to prevent looping the broadcast

    if e.down then
        local dik = e.key
        -- if dik == DIK_SUBTRACT then
        --     startup()
        -- end
        if broadcast == 'on' and not keyboard.modifier() then
            -- TODO incorporate info.menu_open below
            -- local info = ffxi.get_info()
            local target = ffxi.get_mob_by_target('t')

            if (target and target.is_npc) and (dik == DIK_RETURN or dik == DIK_NUMPADENTER) then
                AshitaCore:GetChatManager():QueueCommand(1, '/multisend sendothers /target [t]')
            end
            if
            dik == DIK_DOWN or
            dik == DIK_UP or
            dik == DIK_LEFT or
            dik == DIK_RIGHT or
            dik == DIK_ESCAPE or
            dik == DIK_RETURN or
            dik == DIK_NUMPADENTER or
            dik == DIK_SUBTRACT or
            dik == DIK_ADD or
            dik == DIK_F1 or
            dik == DIK_F8 or
            dik == DIK_MULTIPLY then
                local key = dik_to_key(dik)
                local slot = slots.get_current_window().slot
                local msg = 'broadcast keypress '..key..' '..slot
                if target and target.is_npc and (dik == DIK_RETURN or dik == DIK_NUMPADENTER) then
                    -- delay the send to allow time for others to target the same target
                    ashita.tasks.once(0.1, (function(msg) ipc.send_message(msg, 'others') end):bindn(msg))
                else
                    ipc.send_message(msg, 'others')
                end
            end
        end
    end
end)

ashita.events.register('load', 'broadcast_load_callback', function ()
    broadcast = 'off'
    update_status_indicator()
end)

ashita.events.register('unload', 'broadcast_unload_callback', function ()
    -- remove textures upon unloading
    if AshitaCore:GetPrimitiveManager():Get('repeater_on') then
        AshitaCore:GetPrimitiveManager():Delete('repeater_on')
    end
    if AshitaCore:GetPrimitiveManager():Get('repeater_off') then
        AshitaCore:GetPrimitiveManager():Delete('repeater_off')
    end
end)

-- startup = function()
--     -- launch eliteapi to keep menus in sync
--     args = T{}
--     windower.execute('"'..windower.addon_path..'eliteapi.bat"', args)
-- end

update_broadcast = function(value)
    if value ~= 'on' and value ~= 'off' then return end
    broadcast = value
    update_status_indicator()
end

update_status_indicator = function()
    if not broadcast_status_indicator then
        -- init broadcast status indicator images on first pass
        local image_w = 128
        local image_h = 128
        local render_w = 40
        local render_h = 40
        local pos = {x = scaling.window.w - scaling.scale_f(200), y = scaling.window.h - scaling.scale_f(80)}
        broadcast_status_indicator =
        T{
            on = create_texture_primitive('repeater_on', repeater_on_path, image_w, image_h, render_w, render_h),
            off = create_texture_primitive('repeater_off', repeater_off_path, image_w, image_h, render_w, render_h),
        }
        for _,primitive in broadcast_status_indicator:it() do
            primitive:SetPositionX(pos.x)
            primitive:SetPositionY(pos.y)
        end
    end
    broadcast_status_indicator[broadcast]:SetVisible(true)
    broadcast_status_indicator[(broadcast == 'on') and 'off' or 'on']:SetVisible(false)
end

create_texture_primitive = function(name, image_path, image_w, image_h, render_w, render_h)
    local primitive = AshitaCore:GetPrimitiveManager():Get(name)
    if primitive == nil then
        primitive = AshitaCore:GetPrimitiveManager():Create(name)
    end
    primitive:SetTextureFromFile(image_path)
    primitive:SetWidth(image_w)
    primitive:SetHeight(image_h)
    primitive:SetScaleX(scaling.scale_f(render_w) / image_w)
    primitive:SetScaleY(scaling.scale_f(render_h) / image_h)
    return primitive
end
