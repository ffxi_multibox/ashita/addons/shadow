
require('common')
require('multibox_common')

local _party = T{}

ashita.events.register('command', 'party_command_callback', function (e)
    local args = e.command:args()
    local command = table.remove(args, 1)
    local subcommand = table.remove(args, 1)

    if command == '/shadow' then
        -- assert(subcommand == 'join' or subcommand == 'disband')
        if subcommand == 'join' then
            local player = ffxi.get_player()
            if not player then return end
            ipc.send_message('party join '..player.name)
        elseif subcommand == 'disband' then
            ipc.send_message('party disband')
        end
    end
end)

ipc.register(function(args)
    local command = args[1]
    local subcommand = args[2]

    -- shadow join|disband
    if command == 'party' then
        local player = ffxi.get_player()
        if not player then return end
        local party = ffxi.get_party()

        if subcommand == 'join' then
            local sender = args[3]
            local leader = ''
            if party and party.party1_leader then -- existing party
                leader = ffxi.get_mob_by_id(party.party1_leader)
                if not leader then return end
                leader = leader.name
            else -- new party
                leader = slots.get_names()[1]
            end
            if player.name:lower() ~= leader:lower() then return end

            local names = slots.get_names()
            local total_delay = 0
            for _, name in ipairs(names) do
                if name:lower() ~= player.name:lower() and not _party.is_in_party(name) then
                    log.info('inviting '..name..' to party')
                    ashita.tasks.once(total_delay, (function(name)
                        AshitaCore:GetChatManager():QueueCommand(1, '/pcmd add '..name)
                    end):bindn(name))
                    total_delay = total_delay + 2
                end
            end
        elseif subcommand == 'disband' then
            if party.party1_leader == player.id then
                log.info('disbanding party')
                AshitaCore:GetChatManager():QueueCommand(1, '/pcmd breakup')
            end
        end
    end
end)

function _party.is_in_party(name)
    local party = ffxi.get_party()
    if not party then return false end
    for i=0,5 do
        party_member = party['p'..i]
        if party_member and party_member.name:lower() == name:lower() then
            return true
        end
    end
    return false
end

ashita.events.register('packet_in', 'party_packet_in_callback', function (e)
    -- Look for party request packets..
    if (e.id == 0xDC) then
        -- Obtain the inviter action..
        local sender = struct.unpack('s', e.data, 0x0C + 1);
        log.info('invited by '..sender)

        local player = ffxi.get_player()
        if not player then return end

        local names = slots.get_names()
        for _, name in ipairs(names) do
            if name:lower() == sender:lower() then
                log.info('joining '..sender..'\'s party')
                ashita.tasks.once(1, function()
                    AshitaCore:GetChatManager():QueueCommand(1, '/join')
                end)
                break
            end
        end
    end
end)
