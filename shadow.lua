addon.name      = 'shadow'
addon.author    = 'bambooya'
addon.version   = '0.1'
addon.desc      = 'followers shadow/mimic lead character actions such as following, sneak/invis, mounting, zoning, homepoints/portals, entering/exiting combat, switch target, warp ring, xp ring, cp ring, ...'
addon.link      = 'https://gitlab.com/ffxi_multibox/ashita/addons/shadow'

-- credit to DiscipleOfEris as a significant portion of the follow functionality in this addon was adapted from
-- his [FastFollow](https://github.com/DiscipleOfEris/FastFollow/blob/master/FastFollow.lua) windower addon

require('common')
local primitives = require('primitives')
local scaling = require('scaling')

require('multibox_common')
require('party')
require('broadcast')

local shadow = T{}
local config = T{}

local shadow_enabled = true
local following = false
local leader = nil
local leader_state = nil
local leader_state_queue = Q{}
local follower_history = Q{}
local follower_stuck = false
local follow_status_indicator = T{}
local other_states = T{}
local max_dist = 50.0^2
local warn_dist = 10.0^2
local running = false
local pause = false
local pause_delay = 0.5
local co = nil
local follow_status_green_path = ('%s\\addons\\%s\\%s'):fmt(AshitaCore:GetInstallPath(), addon.name, 'data\\follow_green.png')
local follow_status_yellow_path = ('%s\\addons\\%s\\%s'):fmt(AshitaCore:GetInstallPath(), addon.name, 'data\\follow_yellow.png')
local follow_status_red_path = ('%s\\addons\\%s\\%s'):fmt(AshitaCore:GetInstallPath(), addon.name, 'data\\follow_red.png')
local unloading = false
local rejoin_out_of_range_dist = 5^2 -- the follower will attempt to move within this distance of leader when rejoining from out of range
local shadow_mount = true
local shadow_stealth = true
local shadow_warp = true
local shadow_participants = 'party' -- party | all
local send_all_delay = 0.4
local self_state = nil
local active_state = nil
local mount_name = 'golden bomb'
local zoning_ignore_next_buff_packet = false
local rejoining_from_out_of_range = false
local state_update_interval = 0.1

-- functions
local is_leader
local is_follower
local update_follow_status_indicator
local clear_leader_state
local clear_follower_history
local engage
local disengage
local stop_running
local toboolean
local moving_average_position

-- battle related
STATUS = {IDLE = 0, ENGAGED = 1}
local engaged_delay = 0

local function default_settings()
    local config = T{}

    config.log_level = 'info'
    config.log_console_level = 'warn'

    config = config:merge(slots.default_settings())

    config.follow_dist = 0.8
    config.engaged_dist = 2.0
    config.display = {}
    config.display.pos = {}
    config.display.pos.x = 0
    config.display.pos.y = 20
    config.display.bg = {}
    config.display.bg.red = 0
    config.display.bg.green = 0
    config.display.bg.blue = 0
    config.display.bg.alpha = 102
    config.display.text = {}
    config.display.text.font = 'Consolas'
    config.display.text.red = 255
    config.display.text.green = 255
    config.display.text.blue = 255
    config.display.text.alpha = 255
    config.display.text.size = 10

    return config
end

local follow_dist = nil
local min_dist_engaged = nil
local max_dist_engaged = nil
ashita.events.register('load', 'shadow_load_callback', function ()
    config = settings.load(default_settings(), 'settings', true)
    follow_dist = config.follow_dist^2
    min_dist_engaged = (config.engaged_dist - 0.25)^2
    max_dist_engaged = (config.engaged_dist + 0.25)^2
    log.level = config.log_level
    log.console_level = config.log_console_level
    log.info('loading')
    slots.set_config(config.slot)
    if not AshitaCore:GetPluginManager():IsLoaded('Multisend') then
		log.warn('Warning: shadow addon requires Multisend plugin - https://github.com/ThornyFFXI/Multisend')
	end
    if not AshitaCore:GetPluginManager():IsLoaded('Uberwarp') then
		log.warn('Warning: shadow addon requires Uberwarp plugin - https://github.com/ThornyFFXI/Uberwarp')
	end
end)

ashita.events.register('unload', 'shadow_unload_callback', function ()
    log.info('unloading')
    settings.save('settings')
    unloading = true
    AshitaCore:GetChatManager():QueueCommand(1, '/shadow follow off')
    AshitaCore:GetChatManager():QueueCommand(1, '/shadow broadcast off')
    for n,v in pairs(follow_status_indicator) do
        for c,_ in pairs(v) do
            local name =  n..'_'..c
            local primitive = AshitaCore:GetPrimitiveManager():Get(name)
            if primitive ~= nil then
                AshitaCore:GetPrimitiveManager():Delete(name)
            end
        end
    end
end)

ashita.events.register('command', 'shadow_command_callback', function (e)
    local args = e.command:args()
    local command = table.remove(args, 1)
    local subcommand = table.remove(args, 1)

    if command == '/shadow' then
        -- /shadow toggle - toggles shadow on/off
        -- /shadow on|off - turns shadow on/off
        -- todo finish implementing enable/disable
        --    need to disable status indicators
        --    need to disable on all instances
        if subcommand == 'toggle' then
            shadow_enabled = not shadow_enabled
        elseif subcommand == 'on' then
            shadow_enabled = true
        elseif subcommand == 'off' then
            shadow_enabled = false
        end
        if not shadow_enabled then return end

        -- /shadow follow toggle|on|off [<follower name>] (default 'all') (note that the leader is the character that issued the command)
        if subcommand == 'follow' then
            local subcommand2 = table.remove(args, 1)
            -- /shadow follow toggle [<follower name>] (default 'all') (note that the leader is the character that issued the command)
            if subcommand2 == 'toggle' then
                local player = ffxi.get_player()
                if not player then return end
                local follower = 'all'
                if #args > 0 then
                    follower = table.remove(args, 1)
                end
                if is_leader() then
                    -- follow is on, need to toggle off
                    AshitaCore:GetChatManager():QueueCommand(1, '/shadow follow off '..follower)
                else
                    -- follow is off, need to toggle on
                    AshitaCore:GetChatManager():QueueCommand(1, '/shadow follow on '..follower)
                end
            -- /shadow follow on [<follower name>] (default all) (note that the leader is the character that issued the command)
            elseif subcommand2 == 'on' then
                local player = ffxi.get_player()
                if not player then return end
                local follower = 'all'
                if #args > 0 then
                    follower = table.remove(args, 1)
                    if follower ~= 'all' then
                        follower = T{follower}
                    end
                end
                AshitaCore:GetChatManager():QueueCommand(1, '/formation end')
                ipc.send_message('shadow follow '..player.name, follower)
            -- /shadow follow off [<follower name>] (default all) (note that the leader is the character that issued the command)
            elseif subcommand2 == 'off' then
                local player = ffxi.get_player()
                if not player then return end
                local follower = 'all'
                if #args > 0 then
                    follower = table.remove(args, 1)
                    if follower ~= 'all' then
                        follower = T{follower}
                    end
                end
                ipc.send_message('shadow stop', follower)
            end
        elseif subcommand == 'mount' then
            if self_state.mount then
                AshitaCore:GetChatManager():QueueCommand(1, '/dismount')
            else
                AshitaCore:GetChatManager():QueueCommand(1, '/mount "'..mount_name..'"')
            end
        elseif subcommand == 'hp' or subcommand == 'warp' then
            local msg = 'shadow hp '..get_player_info_message()
            ipc.send_message(msg, get_shadow_participants(), 0)
        elseif subcommand == 'xp' then
            local msg = 'shadow xp '..get_player_info_message()
            ipc.send_message(msg, get_shadow_participants(), 0)
        elseif subcommand == 'cp' then
            local msg = 'shadow cp '..get_player_info_message()
            ipc.send_message(msg, get_shadow_participants(), 0)
        elseif subcommand == 'exit' or subcommand == 'terminate' then
            log.info('exiting')
            settings.save('settings')
            unloading = true
            commands.send_command(1, '/'..subcommand, 'all')
        -- elseif subcommand == 'sendkey' then
        --     local key = args[1]
        --     if key == nil then return end
        --     commands.send_command(1, '/sendkey '..key..' down', T{})
        --     commands.send_command(1, '/sendkey '..key..' up', T{}, 0.1)
        end
    end
end)

local container_id = T{
    inventory = 0,
    mog_safe_1 = 1,
    mog_safe_2 = 2,
    storage = 3,
    mog_locker = 4,
    mog_satchel = 5,
    mog_sack = 6,
    mog_case = 7,
    mog_wardrobe_1 = 8,
    mog_wardrobe_2 = 9,
    mog_wardrobe_3 = 10,
    mog_wardrobe_4 = 11,
    mog_wardrobe_5 = 12,
    mog_wardrobe_6 = 13,
    mog_wardrobe_7 = 14,
    mog_wardrobe_8 = 15,
}

local equipment_slot = T{
    main_weapon = 0,
    sub_weapon = 1,
    range = 2,
    ammo = 3,
    head = 4,
    body = 5,
    hands = 6,
    legs = 7,
    feet = 8,
    neck = 9,
    waist = 10,
    left_ear = 11,
    right_ear = 12,
    left_ring = 13,
    right_ring = 14,
    back = 15,
}

function get_equippable_slots(item)
    local slots = T{}
    for k,v in pairs(equipment_slot) do
        if bit.band(bit.rshift(item.Slots, v), 0x1) > 0 then
            slots[#slots+1] = v
        end
    end

    return slots
end

function is_item_equipped(item)
    local inventory = AshitaCore:GetMemoryManager():GetInventory()
    -- print(item.Id)
    -- print(item.Name[1])
    -- print('slots: '..item.Slots)
    local equippable_slots = get_equippable_slots(item)
    for _,index in ipairs(equippable_slots) do
        local equipment_entry = inventory:GetEquippedItem(index)
        -- print('equip slot: '..equipment_entry.Slot)
        -- print('equip index: '..equipment_entry.Index)
        local container_id = bit.band(bit.rshift(equipment_entry.Index, 8), 0xFF)
        local container_index = bit.band(bit.rshift(equipment_entry.Index, 0), 0xFF)
        -- print('conainer_id: '..container_id)
        -- print('container_index: '..container_index)
        if container_index > 0 then
            local equipped_item = AshitaCore:GetResourceManager():GetItemById(inventory:GetContainerItem(container_id, container_index).Id)
            -- print('checking if '..equipped_item.Name[1]..' is equipped')
            if item.Id == equipped_item.Id then
                -- print(item.Name[1]..' is equipped')
                return true
            end
        end
    end

    return false
end

function find_item(id)
    local containers = {
        container_id.inventory,
        container_id.mog_satchel,
        container_id.mog_sack,
        container_id.mog_case,
        container_id.mog_wardrobe_1,
        container_id.mog_wardrobe_2,
    }
    local inventory = AshitaCore:GetMemoryManager():GetInventory()
    for _,container_id in ipairs(containers) do
        local count = inventory:GetContainerCount(container_id)
        for index=0,count do
            local item = inventory:GetContainerItem(container_id, index)
            if item.Id == id then
                return AshitaCore:GetResourceManager():GetItemById(item.Id)
            end
        end
    end
    return nil
end

function equip_and_use_item(id, slot)
    local item = find_item(id)
    if item ~= nil then -- item exists in inventory
        local item_name = item.Name[1]
        local cast_delay = 0
        if not is_item_equipped(item) then -- item is not equipped
            cast_delay = item.CastDelay + 1.1
            AshitaCore:GetChatManager():QueueCommand(1, '/equip '..slot..' "'..item_name..'"')
        end
        ashita.tasks.once(cast_delay, (function(item_name)
            AshitaCore:GetChatManager():QueueCommand(1, '/item "'..item_name..'" <me>')
        end):bindn(item_name))
    end
end

function do_shadow_hp()
    equip_and_use_item(28540, 'L.ring') -- warp ring
end

function do_shadow_xp()
    equip_and_use_item(27556, 'L.ring') -- echad ring
end

function do_shadow_cp()
    equip_and_use_item(27557, 'L.ring') -- trizek ring
end

local last_debuff_command_time = 0
ipc.register(function(args)
    if ffxi.get_player() == nil then return end
    local info = ffxi.get_info()

    local command = table.remove(args, 1)
    local subcommand = table.remove(args, 1)

    if command == 'shadow' then
        -- notify follower to stop following
        -- sent by: leader, received by: follower
        if subcommand == 'stop' then
            following = false
            leader = nil
            stop_running()
        -- notify follower to start following
        -- sent by: leader, received by: follower
        elseif subcommand == 'follow' then
            local player = ffxi.get_player()
            if not player then return end
            if player.name == args[1] then -- this is the leader
                stop_running()
            end
            following = true
            leader = args[1]
            pause = false
            clear_leader_state()
            clear_follower_history()
        -- update leader/follower knowledge of each other's zone and position
        -- sent by: leader/follower, received by: follower/leader
        elseif subcommand == 'update' then
            if not self_state or unloading then return end

            local state = {
                name=args[1],
                zone=tonumber(args[2]),
                x=tonumber(args[3]),
                y=tonumber(args[4]),
                zoning=toboolean(args[5]),
                time=tonumber(args[6]),
                counter=tonumber(args[7]),
                stuck=toboolean(args[8]),
                mog_house=toboolean(args[9]),
                active=toboolean(args[10]),
                mount=toboolean(args[11]),
                invisible=toboolean(args[12]),
                sneak=toboolean(args[13]),
                recast_invisible=tonumber(args[14]),
                recast_sneak=tonumber(args[15]),
                status=tonumber(args[16]),
                engaged=toboolean(args[17]),
                target=tonumber(args[18]),
                subtarget_active=toboolean(args[19]),
                following=toboolean(args[20]),
                leader=args[21],
            }

            if state.leader == 'nil' then
                state.leader = nil
            end

            if state.active and not window.is_active() then
                active_state = table.clone(state, true)
                if self_state.name ~= active_state.name then
                    self_state.active = false
                end
            end

            other_states[state.name] = state
            if not is_leader() and following and state.name == leader then
                leader_state_queue:push(state)
            end
        elseif subcommand == 'buff' then
            add_or_remove = args[1]
            buff_name = args[2]
            if add_or_remove == 'remove' then
                if not self_state.active
                and self_state.zone == active_state.zone
                and not self_state.mog_house
                and not active_state.mog_house then
                    -- if whm, rdm, or sch, only! cancel sneak if < 180 seconds remain
                    last_debuff_command_time = utils.time()
                    AshitaCore:GetChatManager():QueueCommand(1, '/debuff '..buff_name)
                end
            end
        elseif subcommand == 'uberwarp' then
            sender,target = parse_player_info_message(args)
            local cmd = args:concat(' ')
            if not sender or self_state.zone ~= sender.zone or self_state.mog_house then return end
            if target then
                local dist_sq = distanceSquared(self_state, target)
                if dist_sq < 6^2 then
                    AshitaCore:GetChatManager():QueueCommand(1, cmd)
                end
            else
                local dist_sq = distanceSquared(self_state, sender)
                if dist_sq < (follow_dist+0.5)^2 then
                    AshitaCore:GetChatManager():QueueCommand(1, cmd)
                end
            end
        elseif subcommand == 'hp' then
            sender,_ = parse_player_info_message(args)
            if not sender or self_state.zone ~= sender.zone or self_state.mog_house then return end
            do_shadow_hp()
        elseif subcommand == 'xp' then
            sender,_ = parse_player_info_message(args)
            if not sender or self_state.zone ~= sender.zone or self_state.mog_house then return end
            do_shadow_xp()
        elseif subcommand == 'cp' then
            sender,_ = parse_player_info_message(args)
            if not sender or self_state.zone ~= sender.zone or self_state.mog_house then return end
            do_shadow_cp()
        end
    end
end)

local zone_start_time = nil
local popped_queue = Q{}
local function handle_stuck_follower(popped)
    -- consider follower to be stuck if follower is away from leader or has moved less than the expected threshold
    local leader_time_window = 5 -- sec
    local follower_time_window = 1 -- sec
    local dist_first_to_last_threshold = 1.5
    local zoning_wait_time_for_stuck = 20

    -- is follower close to leader position at the front of queue?
    -- are new states being popped from leader queue or stuck on the same state?
    local close_to_leader = false
    local t = utils.time()
    if popped then
        popped_queue:push(t)
    end
    while popped_queue:length() > 0 and (t - popped_queue[1]) > leader_time_window do
        popped_queue:pop()
    end
    if popped_queue:length() > 0 or distanceSquared(self_state, leader_state) <= follow_dist then
        close_to_leader = true
    end

    -- moving average position over n steps
    n = 3
    pos_avg = (follower_history:length() > 0) and follower_history[follower_history:length()] or self_state
    pos_avg = moving_average_position(pos_avg, self_state, n)

    -- create the follower state and push it onto the follower history queue
    follower_state = {x=pos_avg.x, y=pos_avg.y, time=utils.time(), zone=self_state.zone, zoning=self_state.zoning}
    follower_history:push(follower_state)

    -- compute distance between first and last state of the follower history
    local dist_first_to_last = dist_first_to_last_threshold + 1
    if follower_history:length() > 1 then
        dist_first_to_last = math.sqrt(distanceSquared(follower_history[1], follower_history[follower_history:length()]))
    end

    -- pop follower states older than the moving window off the follower history queue
    if (follower_state.time - follower_history[1].time) > follower_time_window then
        follower_history:pop()
    elseif not close_to_leader then
        -- don't attempt to figure out if the follower is stuck until the follower history is full
        return
    end

    -- prevent stuck state while zoning
    if follower_state.zoning then
        zone_start_time = utils.time()
    end
    if zone_start_time and (utils.time() - zone_start_time) > zoning_wait_time_for_stuck then
        zone_start_time = nil
    end

    if close_to_leader or zone_start_time or leader_state.zone ~= follower_state.zone or (dist_first_to_last > dist_first_to_last_threshold) then
        follower_stuck = false
    else
        follower_stuck = true
    end

    if follower_stuck then
        leader_state_queue:clear()
    end
end

local prev_status = STATUS.IDLE
function update_engaged_state()
    local target = ffxi.get_mob_by_target('t')

    if self_state.active then return end

    -- make sure active is in the same party and zone before engaging in a fight
    if not ffxi.get_mob_by_name(active_state.name)
    or not ffxi.get_mob_by_name(active_state.name).in_party
    or self_state.zone ~= active_state.zone then
        return
    end

    if self_state.status ~= STATUS.ENGAGED and prev_status == STATUS.ENGAGED then
        -- stop auto run and clear leader_state queue
        stop_running()
        clear_leader_state()
    end
    prev_status = self_state.status

    if engaged_delay > utils.time() then return end

    if active_state.engaged and active_state.target then -- active character engaged
        local mob = ffxi.get_mob_by_id(active_state.target)
        if mob and mob.valid_target and mob.spawn_type == 16 and math.sqrt(mob.distance) <= 29.7 then
            if self_state.status ~= STATUS.ENGAGED then -- this character not engaged
                -- print('engaging')
                AshitaCore:GetChatManager():QueueCommand(0, '/attack '..active_state.target)
                engaged_delay = utils.time() + 1.0
            elseif self_state.status == STATUS.ENGAGED then -- this character engaged
                if target and target.id ~= active_state.target and active_state.subtarget_active then -- player target different than leader target and not a subtarget
                    -- print('switching target')
                    AshitaCore:GetChatManager():QueueCommand(0, '/attack '..active_state.target)
                    engaged_delay = utils.time() + 1.0
                end
            end
        end
    else -- active character not engaged
        if self_state.status == STATUS.ENGAGED then -- this character engaged
            -- print('disengaging')
            AshitaCore:GetChatManager():QueueCommand(0, '/attack off')
            engaged_delay = utils.time() + 1.0
        end
    end

    -- turn off target lock if it's enabled
    -- target_locked possible values are nil: no target, false: target unlocked, true: target locked
    if self_state.target_locked then
        AshitaCore:GetChatManager():QueueCommand(0, '/lockon')
        engaged_delay = utils.time() + 1.0
    end
end

function handle_follower_engaged()
    if self_state.engaged and active_state.target then -- this character engaged

        if pause then
            stop_running()
            return false
        end

        local target = nil
        local mob = ffxi.get_mob_by_id(active_state.target)
        -- only pursue target mob if claimed by self/party (red name)
        -- if mob and mob.claim_status == 'party' then -- this doesn't seem to work with trusts
        if mob and mob.claim_id > 0 then -- this works, but does not differentiate mobs claimed by other parties (purple name)
            target = {x=mob.x, y=mob.y}
        else
            return false
        end

        distSq = distanceSquared(target, self_state)
        len = math.sqrt(distSq)
        if len < 1 then len = 1 end

        if (distSq > max_dist_engaged and distSq < max_dist) then
            run((target.x - self_state.x)/len, (target.y - self_state.y)/len)
        elseif (distSq < min_dist_engaged and distSq < max_dist) then
            run((self_state.x - target.x)/len, (self_state.y - target.y)/len)
        elseif (distSq >= min_dist_engaged and distSq <= max_dist_engaged) then
            local target_angle = math.atan2((target.y - self_state.y), (target.x - self_state.x))
            if running then
                stop_running()
            elseif (math.abs(target_angle + self_state.facing) > math.pi / 16.0) then
                turn(-target_angle)
            end
        end
        return true
    end
    return false
end

function handle_follower_disengaged(info)
    -- follower not engaged
    if following then

        if pause then
            stop_running()
            return
        end

        if leader_state_queue:length() == 0 then return end

        local leader_state_next = leader_state_queue[1]
        local popped = false

        -- throw away leader positions from the other zones at the front of the queue
        -- may lose some points after the zone, but hopefully not too many
        while leader_state_queue:length() > 0
        and (self_state.zone ~= leader_state_next.zone
        or self_state.mog_house ~= leader_state_next.mog_house
        or self_state.active) do
            popped = true
            leader_state_next = leader_state_queue:pop()
        end

        if self_state.active then return end

        -- keep the previous leader position unless we're closer than follow_dist or farther than max_dist
        while leader_state_queue:length() > 0
        and not leader_state_queue[1].zoning do
            local dist_sq = distanceSquared(leader_state_next, self_state)
            if dist_sq >= max_dist then rejoining_from_out_of_range = true end
            if dist_sq <= follow_dist or dist_sq >= max_dist then
                popped = true
                leader_state_next = leader_state_queue:pop()
            else
                break
            end
        end

        -- rejoining from out of range - clear the leader queue until follower is within range of current leader position
        if rejoining_from_out_of_range and leader_state_queue:length() > 0 then
            leader_state_next = leader_state_queue[leader_state_queue:length()]
            leader_state_queue:clear()
            local dist_sq = distanceSquared(leader_state_next, self_state)
            if dist_sq <= rejoin_out_of_range_dist then rejoining_from_out_of_range = false end
        end

        -- only use the next leader position if the leader is not transitioning between zones
        if leader_state_next and not leader_state_next.zoning then
            leader_state = leader_state_next
        end

        if not leader_state then return end

        local dist_sq = distanceSquared(leader_state, self_state)
        len = math.sqrt(dist_sq)
        if len < 1 then len = 1 end

        if leader_state.zone == self_state.zone and leader_state.mog_house == self_state.mog_house then
            -- check if follower is stuck
            handle_stuck_follower(popped)

            if dist_sq > follow_dist and dist_sq < max_dist then
                run((leader_state.x - self_state.x)/len, (leader_state.y - self_state.y)/len)
            elseif leader_state_next.zoning and dist_sq < max_dist then
                run()
            elseif running then
                stop_running()
            end
        else
            stop_running()
        end
    end
end

local prev_is_active = false
function handle_active_window_changed()
    if not self_state
    or unloading then
        return
    end

    if not is_leader() then
        if self_state.active and self_state.active ~= prev_is_active then
            -- we just switched to this window so disable auto run and clear leader pos queue
            stop_running()
            rejoining_from_out_of_range = true
        end
        prev_is_active = self_state.active
    end
end

local update_counter = 0
function update_state()
    local mob_me = ffxi.get_mob_by_target('me')
    local player = ffxi.get_player()
    local info = ffxi.get_info()
    local mob_t = ffxi.get_mob_by_target('t')
    local target = AshitaCore:GetMemoryManager():GetTarget()
    local valid = (mob_me and player and info)

    if valid and not self_state then
        local mount = false
        local invisible = false
        local sneak = false
        for _,buff in ipairs(player.buffs) do
            if buff == BUFF_ID.MOUNT then
                mount = true
            elseif buff == BUFF_ID.INVISIBLE then
                invisible = true
            elseif buff == BUFF_ID.SNEAK then
                sneak = true
            end
        end
        self_state = {
            name=player.name,
            zone=info.zone,
            x=mob_me.x,
            y=mob_me.y,
            zoning=false,
            time=utils.time(),
            counter=update_counter,
            stuck=follower_stuck,
            mog_house=info.mog_house,
            active=window.is_active(),
            mount=mount,
            invisible=invisible,
            sneak=sneak,
            recast_invisible=0,
            recast_sneak=0,
            buffs=table.clone(player.buffs),
            status=player.status,
            engaged=(player.status == STATUS.ENGAGED),
            target=mob_t and mob_t.id or nil,
            subtarget_active=(target:GetIsSubTargetActive() ~= 0),
            target_locked=(player.target_locked ~= 0),
            main_job=player.main_job,
            sub_job=player.sub_job,
            facing=mob_me.facing,
            valid=true,
            following=following,
            leader=leader,
        }
    elseif valid and self_state then
        self_state.name = player.name
        self_state.zone = info.zone
        self_state.x = mob_me.x
        self_state.y = mob_me.y
        self_state.time = utils.time()
        self_state.counter = update_counter
        self_state.stuck = follower_stuck
        self_state.mog_house = info.mog_house
        self_state.active = window.is_active() or self_state.active
        self_state.status = player.status
        self_state.engaged = (player.status == STATUS.ENGAGED)
        self_state.target = mob_t and mob_t.id or nil
        self_state.subtarget_active = (target:GetIsSubTargetActive() ~= 0)
        self_state.target_locked = (player.target_locked ~= 0)
        self_state.main_job = player.main_job
        self_state.sub_job = player.sub_job
        self_state.facing = mob_me.facing
        self_state.valid = true
        self_state.following = following
        self_state.leader = leader
    elseif self_state then
        self_state.valid = false
    end
    update_counter = update_counter + 1
end

local last_command_time = {}
local spam_delay = 2
function send_command_without_spamming(command_mode, command)
    local current_t = utils.time()
    last_t = last_command_time[command]
    if not last_t or (current_t - last_t) > spam_delay then
        commands.send_command(command_mode, command)
        last_command_time[command] = current_t
    end
end

local function handle_shadow_mount()
    if not shadow_mount
    or not self_state
    or not active_state
    or self_state.active
    or self_state.zone ~= active_state.zone
    or self_state.mog_house
    or active_state.mog_house then
        return
    end

    if active_state.mount and not self_state.mount then
        send_command_without_spamming(1, '/mount "'..mount_name..'"')
    elseif not active_state.mount and self_state.mount then
        send_command_without_spamming(1, '/dismount')
    end
end

local debuff_delay = 2
local function handle_shadow_stealth()
    if not shadow_stealth
    or not self_state
    or not active_state
    or self_state.active
    or self_state.zone ~= active_state.zone
    or self_state.mog_house
    or active_state.mog_house then
        return
    end

    if self_state.main_job:lower() == 'dnc' or self_state.sub_job:lower() == 'dnc' then
        local job_abilities = res.job_abilities
        local ability_recasts = ffxi.get_ability_recasts()
        -- for _,t in ipairs(ability_recasts) do
        --     print('time remaining: '..t)
        -- end
        local jig_recast_time_remaining = ability_recasts[job_abilities[ABILITY_ID.SPECTRAL_JIG].recast_id]
        if (active_state.invisible and not self_state.invisible)
        or (active_state.sneak and not self_state.sneak) then
            if jig_recast_time_remaining <= 0 then
                local current_t = utils.time()
                if current_t - last_debuff_command_time > debuff_delay then
                    if self_state.sneak then
                        send_command('/debuff sneak')
                    else
                        send_command('/ja "spectral jig" <me>')
                    end
                    last_debuff_command_time = current_t
                end
            end
        end
    elseif self_state.main_job:lower() == 'whm' or self_state.sub_job:lower() == 'whm'
        or self_state.main_job:lower() == 'rdm' or self_state.sub_job:lower() == 'rdm'
        or self_state.main_job:lower() == 'sch' or self_state.sub_job:lower() == 'sch' then
            local spells = res.spells
            local spell_recasts = ffxi.get_spell_recasts()
            local invisible_recast_time_remaining = spell_recasts[spells[SPELL_ID.INVISIBLE].recast_id]
            local sneak_recast_time_remaining = spell_recasts[spells[SPELL_ID.SNEAK].recast_id]
            if active_state.sneak and not self_state.sneak then
                if sneak_recast_time_remaining <= 0 then
                    send_command_without_spamming(1, '/ma "sneak" <me>')
                end
            elseif active_state.invisible and not self_state.invisible then
                if invisible_recast_time_remaining <= 0 then
                    send_command_without_spamming(1, '/ma "invisible" <me>')
                end
            end
    end
end

local function handle_shadow_follow()
    if not self_state
    or not active_state
    or self_state.active
    or unloading then
        return
    end

    update_engaged_state()

    if not handle_follower_engaged() then
        handle_follower_disengaged()
    end
end

local state_update_timer = T{
    last = 0,
    delay = state_update_interval, -- in seconds
}
local function send_state_update()
    if not self_state or unloading then return end

    -- Check if the delay time has passed
    local curr = utils.time()
    local elapsed = curr - state_update_timer.last
    if (elapsed >= state_update_timer.delay) then
        state_update_timer.last = curr

        args = T{
            'shadow update',
            self_state.name,
            self_state.zone,
            self_state.x,
            self_state.y,
            tostring(self_state.zoning),
            self_state.time,
            self_state.counter,
            tostring(self_state.stuck),
            tostring(self_state.mog_house),
            tostring(self_state.active),
            tostring(self_state.mount),
            tostring(self_state.invisible),
            tostring(self_state.sneak),
            self_state.recast_invisible,
            self_state.recast_sneak,
            self_state.status,
            tostring(self_state.engaged),
            tostring(self_state.target),
            tostring(self_state.subtarget_active),
            tostring(self_state.following),
            tostring(self_state.leader),
        }
        ipc.send_message(args:concat(' '), 'all')
    end
end

local function automate()
    -- todo implement this in separate file(s)
end

ashita.events.register('d3d_beginscene', 'shadow_beginscene_callback', function ()
    if not shadow_enabled then return end

    update_state()

    send_state_update()

    handle_active_window_changed()

    handle_shadow_mount()

    handle_shadow_stealth()

    handle_shadow_follow()

    update_follow_status_indicator()

    automate()
end)

PACKET_OUT = { ACTION = 0x01A, USE_ITEM = 0x037, REQUEST_ZONE = 0x05E, ZONE_IN_3 = 0x011, BUFF_CANCEL = 0x0F1, REQUEST_WARP = 0x05B, REQUEST_IN_ZONE_WARP = 0x05C }
PACKET_INC = { ACTION = 0x028, SET_UPDATE = 0x063 }
PACKET_ACTION_CATEGORY = { MAGIC_CAST = 0x03, DISMOUNT = 0x12 }
EVENT_ACTION_CATEGORY = {
    SPELL_FINISH = 4,
    ITEM_FINISH = 5,
    JOB_ABILITY = 6,
    SPELL_BEGIN_OR_INTERRUPT = 8,
    ITEM_BEGIN_OR_INTERRUPT = 9,
    UNBLINKABLE_JOB_ABILTY = 14
}
EVENT_ACTION_PARAM = { BEGIN = 24931, INTERRUPT = 28787 }
BUFF_ID = { INVISIBLE = 69, SNEAK = 71, MOUNT = 252 }
ABILITY_ID = { SPECTRAL_JIG = 196 }
SPELL_ID = { INVISIBLE = 136, SNEAK = 137 }
SLOT = {
    MAIN = 0,
    SUB = 1,
    RANGE = 2,
    AMMO = 3,
    HEAD = 4,
    BODY = 5,
    HANDS = 6,
    LEGS = 7,
    FEET = 8,
    NECK = 9,
    WAIST = 10,
    LEFT_EAR = 11,
    RIGHT_EAR = 12,
    LEFT_RING = 13,
    RIGHT_RING = 14,
    BACK = 15
}

local function is_ma(command)
    return command:sub(1, 3) == '/ma' or command:sub(1, 6) == '/magic'
end

local function is_ra(command)
    return command:sub(1, 3) == '/ra'
end

local function is_item(command)
    return command:sub(1, 5) == '/item'
end

local function recast_time(command)
    local spell_start_index = command:lfind('"', 1, #command)
    local spell_end_index = command:lfind('"', spell_start_index + 1, #command)
    if spell_end_index ~= nil then
        local spell_name = command:sub(spell_start_index + 1, spell_end_index - 1)
        local spell = AshitaCore:GetResourceManager():GetSpellByName(spell_name, 0)  -- langid: 0,1,2 ?
        if spell then
            return AshitaCore:GetMemoryManager():GetRecast():GetSpellTimer(spell.Index)
        end
    end
    return 0
end

local function check_range(command)
    tokens = tokenize(command)
    for i, token in ipairs(tokens) do
        if token == '/ma' or token == '/magic' then
            t = tokens[i+2]
            if t ~= nil then
                local mob_me = ffxi.get_mob_by_target('me')
                local target = ffxi.get_mob_by_target(t) or ffxi.get_mob_by_name(t)
                if mob_me ~= nil and target ~= nil then
                    dist = distanceSquared(mob_me, target)
                    if dist < math.pow(20.7, 2) then
                        return true
                    end
                end
            end
            break
        end
    end
    return false
end

local function ready_for_action(command)
    if is_ma(command) then
        local recast = recast_time(command)
        local in_range = check_range(command)
        -- print('recast: '..tostring(recast))
        -- print('in_range: '..tostring(in_range))
        if in_range and recast <= 0 then
            return true
        else
            return false
        end
    end
    if is_ra(command) then
    end
    if is_item(command) then
    end
    return true
end

local function delayed_action(command)
    if ready_for_action(command) then
        AshitaCore:GetChatManager():QueueCommand(0, '/nodelay '..command)
    else
        pause = false
    end
end

-- filter outgoing text for actions that require temporarily pausing follow (/ma, /ra, /item)
ashita.events.register('text_out', 'shadow_text_out_callback', function (e)
    --[[ Valid Arguments
        e.mode               - (ReadOnly) The message mode.
        e.message            - (ReadOnly) The raw message string.
        e.mode_modified      - The modified mode.
        e.message_modified   - The modified message.
        e.injected           - (ReadOnly) Flag that states if the text was injected by Ashita or an addon/plugin.
        e.blocked            - Flag that states if the text has been, or should be, blocked.
    --]]

    -- e.blocked = false -- don't block by default

    if not shadow_enabled then return end

    if not self_state or self_state.active then return end -- do not modify outgoing text on main

    if not following then return end -- do not modify outgoing text if not following

    -- do not delay, execute the embedded action text immediately
    local nodelay = e.message_modified:sub(1, 8) == '/nodelay'
    if nodelay then
        e.message_modified = e.message_modified:sub(10)
        return
    end

    -- input contains a magic, ranged, or item action
    if (is_ma(e.message_modified) or is_ra(e.message_modified) or is_item(e.message_modified)) and following then
        -- action not ready so don't attempt to execute
        if not ready_for_action(e.message_modified) then
            e.blocked = true -- block / do not execute the input here
            return
        end

        if not pause then
            pause = true
            stop_running()

            -- if co then coroutine.kill() end
            -- co = coroutine.schedule(function()
            --     delayed_action(e.message_modified)
            -- end, pause_delay)
            ashita.tasks.once(pause_delay, (function(message_modified)
                delayed_action(message_modified)
            end):bindn(e.message_modified))

            e.blocked = true -- block / do not execute the input here
            return
        end
    end
end)

function player_has_buff(player, buff_id)
    for _,b in pairs(player.buffs) do
        if b == buff_id then
            return true
        end
    end
    return false
end
function do_homepoint_warp(participants, index)
    local z = nil
    local hp = nil
    local dest = nil
    for zkey,zdata in pairs(homepoints.warpdata) do
        if zdata.index == index then
            z = zkey
            hp = ''
            dest = uberwarp.homepoints[zdata.index].alias
            break
        end
        local match = false
        for hpkey,hpdata in pairs(zdata) do
            if type(hpdata) == 'table' and hpdata.index == index then
                z = zkey
                hp = hpkey == '1' and '' or hpkey
                dest = uberwarp.homepoints[hpdata.index].alias
                match = true
                break
            end
        end
        if match then
            break
        end
    end
    if z ~= nil and hp ~= nil and dest ~= nil then
        print('homepoint warp to '..z..' '..hp)
        ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw hp "'..dest..'"', participants, send_all_delay)
    end
end

function do_unity_warp(participants, index)
    local z = nil
    local dest = nil
    for zkey,zdata in pairs(unity.warpdata) do
        if zdata.index == index then
            if zdata.z ~= nil then
                z = zdata.z
            else
                z = zkey
            end
            -- dest = uberwarp.unity[zdata.index].alias -- uberwarp xml param is not indexed correctly so we use zone name from superwarp instead
            dest = z
            break
        end
    end
    if z ~= nil and dest ~= nil then
        print('unity warp to '..z)
        ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw uc "'..dest..'"', participants, send_all_delay)
    end
end

function do_survival_guide_warp(participants, index)
    local z = nil
    local dest = nil
    for zkey,zdata in pairs(guides.warpdata) do
        if zdata.index == index then
            z = zkey
            dest = uberwarp.guides[zdata.index].alias
            break
        end
    end
    if z ~= nil and dest ~= nil then
        print('survival guide warp to '..z)
        ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw sg "'..dest..'"', participants, send_all_delay)
    end
end

function do_waypoint_warp(participants, option_index)
    _, op_z, op_i = ('b14'):pack(option_index):unpack('b7b4b3')
    local z = nil
    local wp = nil
    local dest = nil
    for zkey,zdata in pairs(waypoints.warpdata) do
        if zdata.op_z == op_z and zdata.op_i == op_i then
            z = zkey
            wp = ''
            dest = uberwarp.waypoints[zdata.index].alias
            break
        end
        local match = false
        for wpkey,wpdata in pairs(zdata) do
            if type(wpdata) == 'table' and wpdata.op_z == op_z and wpdata.op_i == op_i then
                z = zkey
                wp = wpkey
                dest = uberwarp.waypoints[wpdata.index].alias
                match = true
                break
            end
        end
        if match then
            break
        end
    end
    if z ~= nil and wp ~= nil and dest ~= nil then
        print('waypoint warp to '..z..' '..wp)
        ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw wp "'..dest..'"', participants, send_all_delay)
    end
end

function do_escha_enter(participants)
    print('escha enter')
    ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw ee', participants, send_all_delay)
end

function do_escha_exit(participants)
    print('escha exit')
    ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw ee', participants, send_all_delay)
end

function do_escha_warp(participants, index)
    local z = nil
    local p = nil
    local dest = nil
    for zkey,zdata in pairs(escha.warpdata) do
        local match = false
        for pkey,pdata in pairs(zdata) do
            if pdata.index == index then
                z = zkey
                p = pkey
                dest = uberwarp.escha[zdata.index].alias
                match = true
                break
            end
        end
        if match then
            break
        end
    end
    if z ~= nil and p ~= nil and dest ~= nil then
        print('escha warp to portal #'..p..' in '..z)
        ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw ep '..dest, participants, send_all_delay)
    end
end

function do_runic_portal_warp(participants, index)
    if index > 1000 then index = index - 1000 end
    local z = nil
    local dest = nil
    for zkey,zdata in pairs(portals.warpdata) do
        if zdata.index == index then
            z = zkey
            dest = uberwarp.portals[zdata.index].alias
            break
        end
    end
    if z ~= nil and dest ~= nil then
        print('runic portal warp to '..z)
        ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw rp "'..dest..'"', participants, send_all_delay)
    end
end

function do_runic_portal_return(participants)
    print('warning, runic portal return not implemented')
    -- ipc.send_message('shadow uberwarp '..get_player_info_message()..' /uw rp', participants, send_all_delay)
    -- do_runic_portal_warp(participants, 0)
end

function get_shadow_participants()
    members = {}
    if shadow_participants == 'all' then
        members = slots.get_names()
    elseif shadow_participants == 'party' then
        members = get_party_members(slots.get_names())
    else
        print('WARNING: invalid shadow warp participants ('..shadow_participants..').  Expected: all | party.')
    end
    return members
end

function get_shadow_other_participants()
    members = {}
    if shadow_participants == 'all' then
        members = get_others(slots.get_names())
    elseif shadow_participants == 'party' then
        members = get_party_members(get_others(slots.get_names()))
    else
        print('WARNING: invalid shadow warp participants ('..shadow_participants..').  Expected: all | party.')
    end
    return members
end

function get_player_info_message()
    local args = T{'player', self_state.name, self_state.zone, self_state.x, self_state.y}
    local target = ffxi.get_mob_by_target('t')
    if target then
        local target_args = T{'target', target.id, target.x, target.y}
        args:extend(target_args)
    end
    return args:concat(' ')
end

function parse_player_info_message(args)
    local sender = nil
    local target = nil
    if args[1] == 'player' then
        args:remove(1)
        sender = T{name=args[1], zone=tonumber(args[2]), x=tonumber(args[3]), y=tonumber(args[4])}
        args:remove(1)
        args:remove(1)
        args:remove(1)
        args:remove(1)
    end
    if args[1] == 'target' then
        args:remove(1)
        target = T{id=tonumber(args[1]), x=tonumber(args[2]), y=tonumber(args[3])}
        args:remove(1)
        args:remove(1)
        args:remove(1)
    end
    return sender,target
end

function do_shadow_warp(packet)
    if not shadow_warp
    or not self_state.active then
        return
    end

    participants = get_shadow_other_participants()

    local menu_id = packet['Menu ID']
    local zone = packet['Zone']
    local unknown1 = packet['_unknown1']
    local option_index = packet['Option Index']
    local unknown2 = packet['_unknown2']
    local automated_message = packet['Automated Message']

    if zone ~= nil and unknown1 ~= nil and unknown2 ~= nil and option_index ~= nil then
        -- print('menu_id: '..menu_id) -- 3
        -- print('zone: '..zone) -- 3
        -- print('unknown1: '..unknown1) -- 2
        -- print('unknown2: '..unknown2) -- 0
        -- print('option_index: '..option_index) -- 2
        -- print('automated_message: '..tostring(automated_message)) -- 0
        if (menu_id >= 8700 and menu_id <= 8704)
        and automated_message == true
        and option_index == 2
        and unknown2 == 0 then
            -- print('homepoint warp')
            do_homepoint_warp(participants, unknown1)
        elseif (
            menu_id == 598 or -- Igsli
            menu_id == 3529 or -- Urbiolaine
            menu_id == 879 or -- Teldro-Kesdrodo
            menu_id == 879 or -- Yonolala
            menu_id == 5149 -- Nunaarl Bthtrogg
        ) and automated_message == false then
            -- print('unity warp')
            do_unity_warp(participants, option_index)
        elseif (
            menu_id == 8500 or
            menu_id == 8501
        ) and automated_message == false then
            -- print('survival guide warp')
            do_survival_guide_warp(participants, unknown1)
        elseif (
            (menu_id >= 5000 and menu_id <= 5008)
            or menu_id == 10121
        ) and automated_message == true then
            -- print('waypoint warp')
            do_waypoint_warp(participants, option_index)
        elseif (
            menu_id == 65 or -- Qufim
            menu_id == 14 or -- Misareaux
            menu_id == 926 or -- Tahrongi
            menu_id == 222 or -- La Theine
            menu_id == 926 -- Konschtat
        ) and automated_message == true then
            -- print('escha enter')
            do_escha_enter(participants)
        elseif (
            menu_id == 4 or -- Escha - Zi'tah
            menu_id == 1 or -- Escha - Ru'aun
            menu_id == 14 -- Reisenjima
        ) and automated_message == true then
            -- print('escha exit')
            do_escha_exit(participants)
        elseif (
            menu_id == 9100 -- portal/ingress
        ) and automated_message == true then
            -- print('escha warp')
            do_escha_warp(participants, unknown1)
        elseif (
            menu_id == 101 or -- no assaults
            (menu_id >= 120 and menu_id <= 215) -- assaults.
        ) and automated_message == false
        and unknown1 == 0
        and unknown2 == 0
        and option_index > 0
        and zone == 50 then
            -- print('runic portal warp')
            do_runic_portal_warp(participants, option_index)
        elseif (
            menu_id == 131 or -- Leujaoam
            menu_id == 134 or -- Periqia
            menu_id == 109 or -- mamool Ja
            menu_id == 109 or -- Lebros
            menu_id == 109 or -- Ilrusi
            menu_id == 117 or menu_id == 118 -- Nyzul
        ) and automated_message == true
        and unknown1 == 0
        and unknown2 == 0
        and option_index == 0 then
            -- print('runic portal return')
            do_runic_portal_return(participants)
        end
    end
end

ashita.events.register('packet_out', 'shadow_packet_out_callback', function (e)
    if not shadow_enabled then return end

    if not self_state then return false end
    -- print('packet id: '..e.id)

    if e.id == PACKET_OUT.REQUEST_ZONE then
        self_state.zoning = true
        zoning_ignore_next_buff_packet = true
        -- do not suppress zoning
    elseif e.id == PACKET_OUT.ZONE_IN_3 then
        self_state.zoning = false
        -- do not suppress zoning
    elseif e.id == PACKET_OUT.BUFF_CANCEL then
        local buff_id = struct.unpack('B', e.data, 0x04 + 1)
        -- print('buff cancelled: '..res.buffs[buff_id].en)
        if buff_id == BUFF_ID.SNEAK then
            if self_state.active then
                ipc.send_message('shadow buff remove sneak', 'others')
            end
        elseif buff_id == BUFF_ID.INVISIBLE then
            if self_state.active then
                ipc.send_message('shadow buff remove invisible', 'others')
            end
        end
    elseif e.id == PACKET_OUT.REQUEST_WARP then
        -- dialogue choice packet used to select warp choice in dialogue
        local packet = packets.parse_dialogue_choice(e)
        do_shadow_warp(packet)
    end
end)

local function get_buff_updates(packet)
    local player_buffs_prev = table.clone(self_state.buffs)
    self_state.buffs = {}
    local n = 0
    for i=1,32 do
        key = 'Buffs '..tostring(i)
        buff_id = packet[key]
        -- print(tostring(buff_id))
        if buff_id ~= 0xFF and buff_id ~= nil then
            n = n + 1
            self_state.buffs[n] = buff_id
            -- print(res.buffs[buff_id].en)
        end
    end

    local buffs_added = set_diff(self_state.buffs, player_buffs_prev)
    local buffs_removed = set_diff(player_buffs_prev, self_state.buffs)
    -- print('buffs added: ')
    -- for _,buff_id in ipairs(buffs_added) do
    --     print(buff_id)
    --     -- print(res.buffs[buff_id].en)
    -- end
    -- print('buffs removed: ')
    -- for _,buff_id in ipairs(buffs_removed) do
    --     -- print(res.buffs[buff_id].en)
    --     print(buff_id)
    -- end

    return buffs_added, buffs_removed
end

local function handle_buffs(packet)
    if not self_state then return end
    if zoning_ignore_next_buff_packet then
        zoning_ignore_next_buff_packet = false
        return
    end
    -- multibox.log(tostring(packet))
    buffs_added, buffs_removed = get_buff_updates(packet)

    -- update state with added buffs
    for _,buff_id in ipairs(buffs_added) do
        if buff_id == BUFF_ID.INVISIBLE then
            self_state.invisible = true
            if self_state.active then
                self_state.recast_invisible = utils.time()
            end
        elseif buff_id == BUFF_ID.SNEAK then
            self_state.sneak = true
            if self_state.active then
                self_state.recast_sneak = utils.time()
            end
        elseif buff_id == BUFF_ID.MOUNT then
            self_state.mount = true
        end
    end

    -- update state with removed buffs
    for _,buff_id in ipairs(buffs_removed) do
        if buff_id == BUFF_ID.INVISIBLE then
            self_state.invisible = false
        elseif buff_id == BUFF_ID.SNEAK then
            self_state.sneak = false
        elseif buff_id == BUFF_ID.MOUNT then
            self_state.mount = false
        end
    end
end

ashita.events.register('packet_in', 'shadow_packet_in_callback', function (e)
    if not shadow_enabled then return end

    if e.id == PACKET_INC.SET_UPDATE then
        local packet = packets.parse_set_update(e)
        if packet['Order'] == 9 then
            handle_buffs(packet)
        end
    elseif e.id == 0x28 then -- action packet
        local action = packets.parse_action(e)
        local player = ffxi.get_player()
        if not player or action.actor_id ~= player.id then return end
        -- /ma finish or interrupt
        if action.category == EVENT_ACTION_CATEGORY.SPELL_FINISH or (action.category == EVENT_ACTION_CATEGORY.SPELL_BEGIN_OR_INTERRUPT and action.param == EVENT_ACTION_PARAM.INTERRUPT) then
            pause = false
        -- /item finish or interrupt
        elseif action.category == EVENT_ACTION_CATEGORY.ITEM_FINISH or (action.category == EVENT_ACTION_CATEGORY.ITEM_BEGIN_OR_INTERRUPT and action.param == EVENT_ACTION_PARAM.INTERRUPT) then
            pause = false
        -- /ma begin
        elseif action.category == EVENT_ACTION_CATEGORY.SPELL_BEGIN_OR_INTERRUPT and action.param == EVENT_ACTION_PARAM.BEGIN then
        -- /item begin
        elseif action.category == EVENT_ACTION_CATEGORY.ITEM_BEGIN_OR_INTERRUPT and action.param == EVENT_ACTION_PARAM.BEGIN then
        end
    elseif e.id == 0x29 then -- action message packet
        local action_msg = packets.parse_action_message(e)
        local player = ffxi.get_player()
        if not player or action_msg.Actor ~= player.id then return end
        local message_id = action_msg.Message
        if message_id == 17 or message_id == 18 then -- 'Unable to cast spells at this time'
            -- just in case the earlier logic allowed us to attempt to cast a spell when not ready, this will resume following
            pause = false
        elseif message_id == 313 or message_id == 78 then -- '<target name> is out of range.' or '<target name> is too far away'
            pause = false
        elseif message_id == 16 then -- 'casting is interrupted'
            pause = false
        end
    end
end)

function distanceSquared(A, B)
    if not A or not B then print('distanceSquared called from '..tostring(debug.getinfo(2).name)..' ('..tostring(debug.getinfo(2).currentline)..') with A: '..tostring(A)..', B: '..tostring(B)) end
    local dx = B.x-A.x
    local dy = B.y-A.y
    return dx*dx + dy*dy
end

ashita.events.register('key_data', 'shadow_key_data_callback', function (e)
    -- look for numpad (movement) keys
    if e.key == DIK_NUMPAD2 or
       e.key == DIK_NUMPAD4 or
       e.key == DIK_NUMPAD6 or
       e.key == DIK_NUMPAD8 then
        if e.down and not keyboard.modifier() and not is_leader() then
            following = false
            leader = nil
        end
    end
end)

local function create_texture_primitive(name, image_path, image_w, image_h, render_w, render_h)
    local primitive = AshitaCore:GetPrimitiveManager():Get(name)
    if primitive ~= nil then
        AshitaCore:GetPrimitiveManager():Delete(name)
    end
    local primitive = AshitaCore:GetPrimitiveManager():Create(name)
    primitive:SetTextureFromFile(image_path)
    primitive:SetWidth(image_w)
    primitive:SetHeight(image_h)
    primitive:SetScaleX(scaling.scale_f(render_w) / image_w)
    primitive:SetScaleY(scaling.scale_f(render_h) / image_h)
    return primitive
end

local function init_follow_status_indicator(key)
    if follow_status_indicator[key] == nil then
        local image_w = 216
        local image_h = 216
        local render_w = 18
        local render_h = 18
        -- local flags = {draggable = false, bottom = true}
        follow_status_indicator[key] = T{
            green = create_texture_primitive(key..'_green', follow_status_green_path, image_w, image_h, render_w, render_h),
            yellow = create_texture_primitive(key..'_yellow', follow_status_yellow_path, image_w, image_h, render_w, render_h),
            red = create_texture_primitive(key..'_red', follow_status_red_path, image_w, image_h, render_w, render_h),
        }
    end
end

update_follow_status_indicator = function()
    -- print('name: '..name..' zone: '..follower_state.zone..' x,y: '..follower_state.x..','..follower_state.y)

    if unloading then return end

    local mob_me = ffxi.get_mob_by_target('me')
    local info = ffxi.get_info()

    if not mob_me or not info then return end

    if not is_leader() then
        for n,v in pairs(follow_status_indicator) do
            for c,p in pairs(v) do
                p:SetVisible(false)
            end
        end
        return
    end

    local party = ffxi.get_party()
    for i = 0, 17 do
        local party_num = (i / 6):floor() + 1
        local key = T{'p%i', 'a1%i', 'a2%i'}[party_num]:format(i % 6)
        local color_on = nil
        local color_off = {'green', 'yellow', 'red'}
        local party_member = party[key]
        local xoffset = 3540
        local yoffset = 2025
        local pos_base = {-140, -300, -460} -- TODO determine correct alliance position offset (may never need this)
        if party_member ~= nil and party_member.name ~= mob_me.name then
            local follower_state
            if other_states[party_member.name] ~= nil then
                follower_state = table.clone(other_states[party_member.name], true)
            end
            if follower_state and follower_state.following then -- only render status icons for other characters that are following
                -- ui variables
                local party_count = party[('party%i_count'):format(party_num)]
                -- local xind = scaling.menu.w - 142
                -- local yind = scaling.menu.h + pos_base[party_num] + 20 * ((i + 6 - party_count) % 6)
                -- local x = scaling.window.w + scaling.scale_w(-95)
                -- local y = scaling.window.h + scaling.scale_h(-34 - 20 * (6 - i + 1))
                local x = scaling.window.w - scaling.scale_f(142)
                local y = scaling.window.h + scaling.scale_f(pos_base[party_num] + 20 * ((i + 6 - party_count) % 6))

                -- init follow status indicator the first go around
                init_follow_status_indicator(key)

                -- check if the follower has following enabled and is within range
                -- is follower in same zone as leader and is follower following?
                if follower_state.zone == info.zone and follower_state.mog_house == info.mog_house then
                    distSq = distanceSquared(mob_me, follower_state)
                    if follower_state.stuck or distSq > max_dist then
                        color_on = 'red'
                        color_off = {'green', 'yellow'}
                    else
                        color_on = 'green'
                        color_off = {'yellow', 'red'}
                    end
                    local primitive = follow_status_indicator[key][color_on]
                    primitive:SetPositionX(x)
                    primitive:SetPositionY(y)
                    primitive:SetVisible(true)
                end
                for _,c in pairs(color_off) do
                    follow_status_indicator[key][c]:SetVisible(false)
                end
            else
                -- this party member is not following
                if follow_status_indicator[key] ~= nil then
                    for _,p in pairs(follow_status_indicator[key]) do
                        p:SetVisible(false)
                    end
                end
            end
        elseif follow_status_indicator[key] ~= nil then
            -- this handles party members dropping from the party
            for _,p in pairs(follow_status_indicator[key]) do
                p:SetVisible(false)
            end
        end
    end
end

is_leader = function()
    local player = ffxi.get_player()
    if not player then return false end
    for _,s in pairs(other_states) do
        if s.following and s.leader == player.name then
            return true
        end
    end
    return false
end

is_follower = function()
    return following
end

clear_leader_state = function()
    -- clear the leader pos queue so we don't run to stale positions
    leader_state = nil
    if leader_state_queue then
        leader_state_queue:clear()
    else
        leader_state_queue = Q{}
    end
end

clear_follower_history = function()
    follower_stuck = false
    if follower_history then
        follower_history:clear()
    else
        follower_history = Q{}
    end
end

run = function(...)
    if not self_state or not self_state.valid or unloading then return end
    ffxi.run(...)
    running = true
end

turn = function(angle)
    if not self_state or not self_state.valid or unloading then return end
    ffxi.turn(angle)
end

stop_running = function()
    if not self_state or not self_state.valid or unloading then return end
    ffxi.run(false)
    running = false
end

toboolean = function(str)
    return str == "true"
end

moving_average_position = function(pavg, p, n)
    x = pavg.x + (p.x - pavg.x) / n
    y = pavg.y + (p.y - pavg.y) / n
    return {x=x, y=y}
end

function tokenize(command)
    local tokens = {}
    local quote = false
    local buffer = ''
    for token in string.gmatch(command, "[^%s]+") do
        -- keep track of opening/closing quotes and buffer tokens in between
        if token[1] == '"' then
            quote = true
        end
        if token[-1] == '"' then
            quote = false
        end
        if quote then
            buffer = buffer..token
        else
            buffer = token
            table.insert(tokens, buffer)
        end
    end
    return tokens
end

function set_diff(a, b)
    local ab = {}
    for k,v in pairs(a) do ab[v]=true end
    for k,v in pairs(b) do ab[v]=nil end
    local ret = {}
    local n = 0
    for k,v in pairs(a) do
        if ab[v] then
            n = n + 1
            ret[n] = v
        end
    end
    return ret
end

function get_party_members(local_members)
    local members = T{}
    for k, v in pairs(ffxi.get_party()) do
        if type(v) == 'table' then
            if local_members:contains(v.name) then
                members:append(v.name)
            end
        end
    end

    return members
end

function get_others(participants)
    local player = ffxi.get_player().name
    if participants:contains(player) then
        participants:delete(player)
    end

    return participants
end
